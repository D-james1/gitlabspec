

Pod::Spec.new do |s|


  s.name         = "myDemo"
  s.version      = "1"
  s.summary      = "this is demo"


  s.description  = <<-DESC
  					Masonry is a light-weight layout framework which wraps AutoLayout with a nicer syntax. Masonry has its own layout DSL which provides a chainable way of describing your NSLayoutConstraints which results in layout code that is more concise and readable. Masonry supports iOS and Mac OS X.
                   DESC

  s.homepage     = "https://gitlab.com/D-james1/mydemo"

  s.license      = "MIT License"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  s.author       = { "段盛武" => "duanshengwu@jsj.com.cn" }

  s.platform     = :ios, "9.0"


  s.source       = { :git => "https://gitlab.com/D-james1/mydemo.git", :tag => s.version }



  s.source_files  = "myDemo/myDemo/myDemo/*.{h,m}"
  # s.exclude_files = "myDemo/Exclude"

  # s.public_header_files = "Classes/**/*.h"


  # s.resource  = "icon.png"
  # s.resources = "myDemo/myDemo/**/*.{storyboard,xib}", "myDemo/myDemo/Assets.xcassets"


  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
